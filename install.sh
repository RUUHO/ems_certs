sudo mkdir -p /usr/local/share/ca-certificates/ems/
for file in *.cer; do
    sudo cp "$file" "/usr/local/share/ca-certificates/ems/$(basename "$file" .cer).crt"
done
sudo update-ca-certificates
